<!DOCTYPE html>
<html>
    <head>
        <title>Lab 1</title>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <style>
            table, td, th { border: 1px solid gray }
        </style>
    </head>
    <body>   
    	<h1>Hello World</h1>
    	
    	<h2><a href="page2.php">Page 2</a></h2>
    	
    	<!-- Unordered List -->
    	
    	<ul>
    	    <li>This</li>
    	    <li>is</li>
    	    <li>a</li>
    	    <li>list</li>
    	</ul>
    	
    	 <!--Table -->
    	
        <table>
            <tr>
                <th colspan="2">This is a table</th>
            </tr>
            <tr>
                <td>Row 1, Column 1</td>
                <td>Row 1, Column 2</td>
            </tr>
            <tr>
                <td>Row 2, Column 1</td>
                <td>Row 2, Column 2</td>
            </tr>
        </table>
    	
    	<!-- Picture -->
    	
    	<img src="Systems%20Analysis.png" width="500" height="500" style="margin:10px 0px" alt="Message notation systems analysis">
    	
    	<!-- Address Block -->
    	
    	<address>
            Written by <a href="mailto:kevink@websforme.com.au">Kevin Kirby</a>.<br> 
            Visit us at:<br>
            <a href="http://websforme.com.au">WebsForMe</a><br>
            Box 564, Disneyland<br>
            USA
        </address>
        
    </body>
</html>
