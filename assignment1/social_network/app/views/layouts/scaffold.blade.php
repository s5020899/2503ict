<!doctype html>
<html>
	<head>
		@include('includes.head')
	</head>

	<body>

		<div class="container">
			@if (Session::has('message'))
				<div class="flash alert">
					<p>{{ Session::get('message') }}</p>
				</div>
			@endif
			@include('includes.nav')
			@yield('main')
		</div>

	</body>

</html>