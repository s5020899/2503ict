@extends('layouts.scaffold')

@section('main')

<h1>Create Tweet</h1>

{{ Form::open(array('route' => 'tweets.store')) }}
<form class="create">
    <div class="form-group">
        {{ Form::label('author', 'Author:') }}
        {{ Form::text('author', null, array('class' => 'createtext')) }}
    </div>
    <div class="form-group">
        {{ Form::label('title','Title:') }}
        {{ Form::text('title', null, array('class' => 'createtext')) }}
    </div>
    <div class="form-group">
        {{ Form::label('body', 'Body:') }}
        {{ Form::textarea('body', null, ['class' => 'col-xs-12']) }}
    </div>
{{ Form::submit('Submit', array('class' => 'createsub btn btn-info')) }}
</form>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


