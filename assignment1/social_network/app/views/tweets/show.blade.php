@extends('layouts.scaffold')

@section('main')

<h1>Show Tweet</h1>

<p>{{ link_to_route('tweets.index', 'Return to all tweets') }}</p>

<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>Author</th>
			<th>Body</th>
			<th colspan="2">Edit / Delete</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td class="name"><b>Name: </b> {{{ $tweet->author }}} {{ HTML::image('images/technics.jpg', 'technics', array( 'width' => 84, 'height' => 84 )) }}</td>
			<td><p><b>Title: </b>{{{ $tweet->title }}}</p>
			<p><b>Message:</b> {{{ $tweet->body }}}</p></td>
            <td>{{ link_to_route('tweets.edit', 'Edit', array($tweet->id), array('class' => 'btn btn-info')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('tweets.destroy', $tweet->id))) }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>
<?php $id = $tweet->id ?>
@include('comments.create')
@stop

