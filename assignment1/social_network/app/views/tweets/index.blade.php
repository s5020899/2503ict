@extends('layouts.scaffold')

@section('main')

<h1>All Tweets</h1>

<p>{{ link_to_route('tweets.create', 'Add new tweet') }}</p>

@if ($tweets->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Author</th>
				<th>Posts</th>
				<th colspan="2">Edit / Delete</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($tweets as $tweet)
				<tr>
					<td class="name"><b>Name: </b> {{{ $tweet->author }}} {{ HTML::image('images/technics.jpg', 'technics', array( 'width' => 84, 'height' => 84 )) }}</td>
					<td><b>Title:</b> {{{ $tweet->title }}}<p>
					<p><b>Message:</b> {{{ $tweet->body }}}</p>
					<p>{{ link_to_route('tweets.show', 'Comments', array($tweet->id), array('class' => 'btn btn-info')) }}</p></td>
                    <td class="col-xs-1">{{ link_to_route('tweets.edit', 'Edit', array($tweet->id), array('class' => 'btn btn-info')) }}</td>
                    <td class="col-xs-1">
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('tweets.destroy', $tweet->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no tweets
@endif

@stop
