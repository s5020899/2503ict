@extends('layouts.scaffold')

@section('main')

<h1>Edit Tweet</h1>
{{ Form::model($tweet, array('method' => 'PATCH', 'route' => array('tweets.update', $tweet->id))) }}
<form class="create">
    <div class="form-group">
        {{ Form::label('author', 'Author:') }}
        {{ Form::text('author', null, array('class' => 'createtext')) }}
    </div>
    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'createtext')) }}
    </div>
    <div class="form-group">
        {{ Form::label('body', 'Body:') }}
        {{ Form::textarea('body', null, ['class' => 'col-xs-12']) }}
    </div>
		{{ Form::submit('Update', array('class' => 'createsub btn btn-info')) }}
		{{ link_to_route('tweets.show', 'Cancel', $tweet->id, array('class' => 'btn')) }}
</form>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
