<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/lu.php";


// Display search form
Route::get('/', function()
{
	return View::make('lu.query');
});

// Perform search and display results
Route::get('search', function()
{
  $search = Input::get('search');

  $results = search($search);

	return View::make('lu.results')->withlu($results)->withSearch($search);
});


/* Functions for la database example. */

/* Search sample data for $name or $year or $state from form. */
function search($search) {
  $lu = getlu();

  // Filter $lu by $name
  if (!empty($search)) {
    $results = array();
  	foreach ($lu as $la) {
  	    if (stripos($la['name'], $search) !== FALSE) {
  		    $results[] = $la;
  	    }
  	    else if (stripos($la['address'], $search) !== FALSE) {
  	      $results[] = $la;  
  	    }
  	    else if (stripos($la['phone'], $search) !== FALSE) {
  	      $results[] = $la;
  	    }
  	    else if (stripos($la['email'], $search) !== FALSE) {
  	      $results[] = $la;
  	    }
    }
    $lu = $results;
  }

  return $lu;
}