<!DOCTYPE html>
<!-- Products. -->

<html>
	<head>
	    <title>@yield('title')</title>
	    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
	    <meta charset="utf-8">
	    <meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	    
	    <!-- Latest compiled and minified CSS -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	    <!-- Latest compiled and minified JavaScript -->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

		<style>
			table form { margin-bottom: 0; }
			.create { position:relative; }
			.createtext { position: absolute; left: 10em; }
			.createsub { margin: 10px 0; }
			form text { margin-left:20px; }
			td body { text-align: left;}
			.name {text-align:left;}
			.name img { float: right; }
			form ul { margin-left: 0; list-style: none; }
			.error { color: red; font-style: italic; }
		</style> 
		
	</head>
	<body>
		<div class="container container-fluid" style="padding:10px;">
			@if (Auth::check())
			 	{{ Auth::user()->username}}
			 	{{ link_to_route('user.logout', 'Logout') }}
			 @else
			 	{{ Form::open(array('route' => 'user.login'))  }}
			 	<form class="well">
				    <div class="form-group">
				        {{ Form::label('username', 'Username:') }}
				        {{ Form::text('username', null) }}
				        {{ $errors->first('username','<small class=error>:message</small>') }}
				    </div>
				    <div class="form-group">        
				        {{ Form::label('password', 'Password:') }}
				        {{ Form::password('password', null) }}
				        {{ $errors->first('password', '<small class=error>:message</small>') }}
				    </div>
				        {{ Form::submit('Login', array('class' => 'createsub btn btn-info')) }}
						{{ link_to_route('user.create', 'Create an Account', array(), array('class' => 'btn btn-info')) }}
				</form>
				{{ Form::close() }}
			@endif
			@yield('content')
		</div>
	</body>
</html>