@extends('product.layout')
@section('content')

@foreach($products as $product)
<table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Price</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ link_to_route('product.show', $product->name, array($product->id)) }}</td>
            <td>{{{ $product->price }}}</td>
        </tr>
    </tbody>
</table>
@endforeach
@stop