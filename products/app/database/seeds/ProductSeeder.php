<?php

class ProductSeeder extends Seeder {
    
    public function run()
    {
        $product = new Product;
        $product->name = 'Ginger Beer';
        $product->price = 3.5;
        $product->save();
        
        $product = new Product;
        $product->name = 'Pepsi Max';
        $product->price = 3;
        $product->save();
    }
}