<?php
	$date = new DateTime();
	$newDate = $date->format('Y-m-d H:i:s');

	$posts = array(
		array(
		    'date' => '27 Jan 2013', 
		    'message' => 'Hi!', 
		    'image' => 'hi.jpg'),
		array(
		    'date' => "$newDate", 
		    'message' => 'Goodbye!', 
		    'image' => 'bye.jpg'),
		array(
		    'date' => '27 Jan 2016',
		    'message' => 'Just testing',
		    'image' => 'testing.jpg'),
	);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lab 2</title>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    </head>

    <body> 
        <div id="container" style="margin: 10px auto; position: relative" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <nav class="navbar navbar-default col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="container-fluid">
                    <a class="navbar-brand col-xs-6 col-sm-6" href="#">Social Network</a>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="col-xs-2 col-sm-3"><a href="#">Photos</a></li>
                        <li class="col-xs-2 col-sm-3"><a href="#">Friends</a></li>
                        <li class="col-xs-2 col-sm-3"><a href="#">Login</a></li>
                    </ul>
                </div>
            </nav>
            <div id="photo" style="width: 230px;">
                <img src="the-princess-gets-a-kiss.jpg" width="120" height="90" style="margin: 10px;" alt="the-princess-gets-a-kiss">
                <h5 style="float: right; margin-top: 20px">Kevin Kirby</h5>
            </div>
            <div id="posts" style="position: relative; top:-40px; left: 20%; width:60%;">
               <?php foreach ($posts as $post) { ?>
                    <div class="post" style="margin: 20px; border:2px solid black; padding: 10px; position: relative; min-height: 105px">
                        <div class="details">    
                            <p><?= $post['date'] ?></p>
                            <p><?= $post['message'] ?></p>
                        </div>
                        <div class="image" style="position: absolute; right: 50px; top: 10px">
                            <img src="<?= $post['image'] ?>" alt="<?= $post['image'] ?>" width="80" height="80">
                        </div>
                    </div>
               <?php } ?>
            </div>
        </div>
    </body>
</html>
