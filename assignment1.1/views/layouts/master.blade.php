
<!-- display profile -->
<!DOCTYPE html>
<!-- Profile page with recent posts. -->

<html>
<head>
	@include('includes.head')
</head>
	<body>
		<div class="container container-fluid col-xs-12 col-sm-12 col-md-12 col-lg-12">
			@yield('content')
		</div>
	</body>
</html>
