@extends('layouts.master') 

@section('title')
News Feed
@stop

@section('content')

@include('includes.nav')

    <h3>View All Tweets</h3>
        <div id="posts" class="col-xs-12" style="padding:10px;">
        <?php foreach ($posts as $post) { ?>
            <table class="table table-hover col-xs-12">
                <tbody>
                    <tr><th class="col-xs-1">{{ HTML::image('images/technics.jpg', 'technics', array( 'width' => 84, 'height' => 84 )) }}</th><td></td></tr>
                    <tr><th class="col-xs-1">Author: </th><td><?= $post['author'] ?></td></tr>
                    <tr><th class="col-xs-1">Title: </th><td><?= $post['title'] ?></td></tr>
                    <tr><th class="col-xs-1">Message: </th><td><?= $post['message'] ?></td></tr>
                    <tr><th class="col-xs-1"><a href="#">Edit</a></th><td><b><a href="#">Delete</a></b></td></tr>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>

@stop


