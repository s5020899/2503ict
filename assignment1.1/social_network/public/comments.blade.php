@extends('layouts.master') 

@section('title')
Comments Page
@stop

@section('content')

@include('includes.nav')

    <h3 style="padding:10px;">Post with Comments</h3>
        <div id="posts" class="col-xs-12" style="padding:10px;">
           @if($posts)
           @foreach($posts as $posted) 
            <table class="table table-hover">
                <tbody class="col-xs-12">
                    <tr><th class="col-xs-1">{{ HTML::image('images/technics.jpg', 'technics', array( 'width' => 84, 'height' => 84 )) }}</th><td></td></tr>
                    <tr><th class="col-xs-1">Author: </th><td>{{{ $posted->posts_author }}}</td></tr>
                    <tr><th class="col-xs-1">Title: </th><td>{{{ $posted->posts_title }}}</td></tr>
                    <tr><th class="col-xs-1">Message: </th><td>{{{ $posted->posts_message }}}</td></tr>
                </tbody>
            </table>
            @endforeach
            <hr>
            <h3 style="padding:10px;">Comments on Post</h3>
            @if($comments)
            @foreach($comments as $comment) 
            <table class="table table-hover">
                <tbody class="col-xs-12">
                    <tr><th class="col-xs-1">Author: </th><td>{{{ $comment->comments_author }}}</td></tr>
                    <tr><th class="col-xs-1">Message: </th><td>{{{ $comment->comments_message }}}</td></tr>
                    <tr></tr>
                </tbody>
            </table>
            @endforeach
            @else
            <p>No items found.</p>
            @endif
            <hr>
            $foreach($posts as $post)
            <form class="create" method="post" action="{{{ url ("comment_action/$posted->id") }}}">
                <div class="form-group">
                    <label for="author">Author: </label>
                    <input type="text" class="createtext col-xs-5" name="author" placeholder="David">
                </div>
                <div class="form-group">
                    <label for="message">Message: </label>
                    <textarea class="form-control" rows="4" name="message" placeholder="It was a great day for a swim! :)"></textarea>
                </div>
                    <input type="submit" value="Add Post" class="createsub btn btn-info"></td></tr>
                </table>
            </form> 
            @endforeach
                    @else
        <p>No items found.</p>
        @endif
        </div>
    </div>

@stop