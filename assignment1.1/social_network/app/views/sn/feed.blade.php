@extends('layouts.master') 
@section('title')
News Feed
@stop
@section('content')
<h3 style="margin:10px;">Create Post</h3>

                        <!--Form to add a new post-->
                        
                        <form style="margin:0px 10px" class="create well" method="post" action="{{{ url ('add_post_action') }}}">
                            <div class="form-group">
                                <label>Author: </label>
                                <input type="text" class="createtext col-xs-5" name="author">
                            </div>
                            <div class="form-group">
                                <label>Title: </label>
                                <input type="text" class="createtext col-xs-5" name="title">
                            </div>
                            <div class="form-group">
                                <label>Message: </label>
                                <textarea class="form-control" rows="4" name="message"></textarea>
                            </div>
                            <button type="submit" class="createsub btn btn-info">Add Post</button>
                        </form>
<h3 style="margin:10px;">News Feed</h3>
                        <div class="col-xs-12" style="padding:10px;">
                            @if ($posts)
                            
                            <!--Loop for each Post in Posts Table -->
                            
                            @foreach($posts as $post) 
                                <table class="table table-hover">
                                    <tbody class="col-xs-12">
                                        <tr><th class="col-xs-1">{{ HTML::image('images/technics.jpg', 'technics', array( 'width' => 84, 'height' => 84 )) }}</th><td></td></tr>
                                        <tr><th class="col-xs-1">Author: </th><td>{{{ $post->posts_author }}}</td></tr>
                                        <tr><th class="col-xs-1">Title: </th><td>{{{ $post->posts_title }}}</td></tr>
                                        <tr><th class="col-xs-1">Message: </th><td>{{{ $post->posts_message }}}</td></tr>
                                        <tr><td class="col-xs-1"><b><a href="{{{ url("edit_post/$post->id") }}}">Edit</a></b></td>
                                            <td style="width:30em"><b><a href="{{{ url("delete_post_action/$post->id") }}}">Delete</a><a style="padding-left:1em" href="{{{ url("comment_on_post/$post->id") }}}">View {{{ get_comment_count($post->id) }}} Comments</a></b></td>
                                            </tr>
                                    </tbody>
                                </table>
                            @endforeach
                            @else
                            <p>It's like a ghost town here.</p>
                            @endif
</div>
@stop