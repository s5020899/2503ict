@extends('layouts.master') 
@section('title')
Add Comment
@stop
@section('content')
        <div class="col-xs-12" style="padding:10px;">
            <table class="table table-hover">
                <tbody class="col-xs-12">
                    <tr><th class="col-xs-1">{{ HTML::image('images/technics.jpg', 'technics', array( 'width' => 84, 'height' => 84 )) }}</th><td></td></tr>
                    <tr><th class="col-xs-1">Author: </th><td>{{{ $post->posts_author }}}</td></tr>
                    <tr><th class="col-xs-1">Title: </th><td>{{{ $post->posts_title }}}</td></tr>
                    <tr><th class="col-xs-1">Message: </th><td>{{{ $post->posts_message }}}</td></tr>
                </tbody>
            </table>
            <hr>
            <h3 style="padding:10px;">Comment on Post</h3>
            <form class="create well" method="post" action="{{ url ("comment_action/$post->id") }}">
                <div class="form-group">
                    <label>Author: </label>
                    <input type="text" class="createtext col-xs-5" name="author">
                </div>
                <div class="form-group">
                    <label>Message: </label>
                    <textarea class="form-control" rows="4" name="message"></textarea>
                </div>
                <button type="submit" class="createsub btn btn-info">Add Comment</button>
            </form> 
            <h3 style="padding:10px;">Comments</h3>
            @if($comments)
            @foreach($comments as $comment) 
            <table class="table table-hover">
                <tbody class="col-xs-12">
                    <tr><th class="col-xs-1">Author: </th><td>{{{ $comment->comments_author }}}</td></tr>
                    <tr><th class="col-xs-1">Message: </th><td>{{{ $comment->comments_message }}}</td></tr>
                    <tr><th class="col-xs-1"><a href="{{{ url("delete_comment/$comment->id/$comment->postsid") }}}">Delete</a></th><td></td></tr>
                </tbody>
            </table>
            @endforeach
            @else
            <p>No comments yet.</p>
            @endif
</div>
@stop

