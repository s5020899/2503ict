@extends('layouts.master')
@section('title')
Edit Post
@stop
@section('content')
<h3 style="margin:10px;">Edit Post</h3>
<form class="create well" method="post" action="{{{ url("edit_post_action/$post->id") }}}">
    <div class="form-group">
        <label>Title: </label>
        <input type="text" class="createtext col-xs-5" name="title" value="{{{ $post->posts_title }}}">
    </div>
    <div class="form-group">
        <label>Message: </label>
        <textarea class="form-control" rows="4" name="message">{{{ $post->posts_message }}}</textarea>
    </div>
        <button type="submit" class="createsub btn btn-info">Save</button><a style="margin-left:10px" class="createsub btn btn-info" href="{{{ url("feed") }}}">Cancel</a>
</form>
@stop