@extends('layouts.master')

@section('title')
Documentation
@stop
@section('content')

<h1>Documentation</h1>

<h4>Entity Relationship Diagram</h4>
{{ HTML::image('images/entity_relationship_diagram.png', 'entity relationship diagram', array( 'width' => 375, 'height' => 200 )) }}

<h4>Website Routes</h4>
{{ HTML::image('images/website_diagram.png', 'website diagram', array( 'width' => 370, 'height' => 240 )) }}

<ol style="margin:10px">
    <li>I was unable to complete any <b>form validation</b> at this point.</li>
    <li><b>Approach I took: </b>I beleieve the majority of this assignment was achieved by reading lecture notes and completing each lab</li>
    <li><b>Extra functionality: </b>Deleteing a post will delete related comments</li>
    <li>I believe I have satisified <b>all</b> of the requirements listed below</li>
</ol>
<ol style="margin:0px 10px 10px 10px">
    <li>All pages must have a navigation menu, either across the top of the page or down the left or right column.</li>
    <li>The home page must display a form for the user to create a new post.</li>
    <li>The home page must also display all posts, in reverse-order of creation (most recent posts first).</li>
    <li>Each post should contain an icon (each post can have the same icon).</li>
    <li>Each post should contain a title and a message and a user’s name (the user is not required to login, they can simply enter their name in the post form).</li>
    <li>Each post should indicate the number of comments.</li>
    <li>Each post should have edit and delete buttons or links.</li>
    <li>Each post should have a view comments link.</li>
    <li>When the edit button is pressed an edit page should be displayed which presents a form to edit the contents of the post. A cancel button on this page simply redirects back to the home page without any changes. A save button, updates the post and redirects to the page below.</li>
    <li>When the view comments link is pressed, a new page is displayed showing the post as well as all of the comments.</li>
    <li>The view comments page should contain a form to add a new comment to the post.</li>
    <li>Comments have a message and a user, but no title.</li>
    <li>Comments have a delete button but no edit button. Deleting a comment should redirect to the view comments page.</li>
</ol>
@stop