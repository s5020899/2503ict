<!DOCTYPE html>
<!-- News feed with recent posts. -->

<html>
	<head>
	    <title>@yield('title')</title>
	    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
	    <meta charset="utf-8">
	    <meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	    
	    <!-- Latest compiled and minified CSS -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	    <!-- Latest compiled and minified JavaScript -->
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		
		<style>
			table form { margin-bottom: 0; }
			.create { position:relative; }
			.create input { width: 10em; }
			.createtext { position: absolute; left: 6em; }
			.createsub { margin: 10px 0; }
			form text { margin-left:20px; }
			td body { text-align: left;}
			.name {text-align:left;}
			.name img { float: right; }
			form ul { margin-left: 0; list-style: none; }
			.error { color: red; font-style: italic; }
		</style>  
	</head>
	<body>
		<div class="container container-fluid" style="padding:10px;">
			<nav class="navbar navbar-default navbar-static-top">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="{{{ url("feed") }}}">Social Network</a>
			    </div>
			
			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <ul class="nav navbar-nav navbar-right">
			        <li class="dropdown">
			          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu <span class="caret"></span></a>
			          <ul class="dropdown-menu">
			            <li><a href="{{{ url("feed") }}}">News Feed</a></li>
			            <li><a href="#">Profile</a></li>
			            <li><a href="#">Friends</a></li>
			            <li role="separator" class="divider"></li>
			            <li><a href="{{{ url("docs") }}}">Documentation</a></li>
			          </ul>
			        </li>
			      </ul>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
			@yield('content')
		</div>
	</body>
</html>