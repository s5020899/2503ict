@extends('layouts.master')

@section('content')

    {{ Form::open(['route' => 'sessions.store', 'class' => 'well']) }}
        <div class="form-group">
            {{ Form::label('author', 'Author:') }}
            {{ Form::text('author', null, ['class' => 'form-control']) }}
            {{ $errors->first('author', '<span class="error">:message</span>') }}
        </div>
        <div class="form-group">
            {{ Form::label('title', 'Title:') }}
            {{ Form::text('title', null, ['class' => 'form-control']) }}
            {{ $errors->first('title', '<span class="error">:message</span>') }}
        </div>
        <div class="form-group">
            {{ Form::label('message', 'Message:') }}
            {{ Form::textarea('message', null, ['class' => 'form-control']) }}
            {{ $errors->first('message', '<span class="error">:message</span>') }}
        </div>
                                    <!-- Log In Field -->
        <div class="form-group">
            {{ Form::submit('Log In', ['class' => 'btn btn-primary']) }}
        </div>
    {{ Form::close() }}

@stop