<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::resource('sessions', 'SessionsController');

Route::get('/', function()
{
	$posts = get_posts();		// Function call to get all Posts
  return View::make('sn.feed')->withPosts($posts);
});

Route::get('feed', function()
{
  $posts = get_posts();		// Function call to get all Posts
  return View::make('sn.feed')->withPosts($posts);
});

Route::get('create', function()
{
  return View::make('sn.create');
});

Route::get('docs', function()
{
	return View::make('sn.docs');
});

Route::get('delete_comment/{id}/{postsid}', function($id,$postsid)
{
  delete_comment($id);		            // Function call to DELETE a comment
  $post = get_post_2($postsid);				// Function call to get the post the comment was deleted from
  $comments = get_comments($id);      // Function call to get comments related to that particular post
  return Redirect::to(url("comment_on_post/{$postsid}"))->withComments($comments)->withPosts($post);
});

Route::post('add_post_action', function()
{
  $author = Input::get('author');
  $title = Input::get('title');
  $message = Input::get('message');
  $test = add_post($author, $title, $message); 		// Function call to add a new post

  return Redirect::to(url("feed"));
});

Route::get('edit_post/{id}', function($id)
{
  $post = get_post($id);		// Function call to get a post that is to be commented on
  return View::make('sn.edit')->withPost($post);
});

Route::post('edit_post_action/{id}', function($id)
{
  $title = Input::get('title');
  $message = Input::get('message');
  $id = edit_post($title, $message, $id);		// Function call to edit a post
  return Redirect::to(url("comment_on_post/{$id}"));
});

Route::get('delete_post_action/{id}', function($id)
{
  delete_post($id);		// Function call to delete a post
  return Redirect::to(url("feed"));
});

// This was for testing to be sure all Posts are returned counted, and in decending order.

// Route::get('posts', function()
// {
// 	$sql = 'SELECT * FROM Posts ORDER BY id desc';
// 	$results = DB::SELECT($sql);
// 	$posts = $results;
// 	$count = count($posts);
// 	foreach($posts as $post)
// 	{
// 		$author = $post->posts_author;
// 		$title = $post->posts_title;
// 		$message = $post->posts_message;
// 		echo "Author: $author, message: $message, Number of posts: $count "."\n";
// 	}
// });

Route::get('comment_on_post/{id}', function($id)
{
  $post = get_post($id);					// Function call to get a post
  $comments = get_comments($id);	// Function call to get comments related to a particular post
  return View::make('sn.add_comment')->withPost($post)->withComments($comments);
});

Route::post('comment_action/{id}', function($id)
{
	$author = Input::get('author');
  $message = Input::get('message');
	$test = add_comment($id, $author, $message);		// Function call to add a comment
	$posts = get_post($id);													// Function call to get post
	$comments = get_comments($id);									// Function call to get all comments related to a post 	
	return Redirect::to(url("comment_on_post/{$id}"))->withComments($comments)->withPosts($posts);
});


// Get Comments Related to Postsid ORDER BY desc
function get_comments($id)
{
	$sql = "SELECT * FROM Comments WHERE postsid = ? ORDER BY id desc";
	$comments = DB::SELECT($sql, array($id));
	return $comments;
}
// Get the number of comments for a given post
function get_comment_count($id)
{
	$sql = "SELECT * FROM Comments WHERE Comments.postsid = ?";
	$comments = DB::SELECT($sql, array($id));
	$count = count($comments);
	return $count;
}

// Add a comment to a post
function add_comment($id, $author, $message)
{
	$sql = "INSERT into Comments (postsid, comments_author, comments_message) values (?, ?, ?)";
  $test = DB::INSERT($sql, array($id, $author, $message));
  return $test;
}

// Edit a post
function edit_post($title, $message, $id)
{
  $sql = "UPDATE Posts SET posts_title = ?, posts_message = ? WHERE id = ?"; 
  DB::UPDATE($sql, array($title, $message, $id));
  return $id;
}

// Get a post that is to be commented on
function get_post($id)
{
	$sql = "SELECT id, posts_author, posts_title, posts_message FROM Posts WHERE id = ?";
	$posts = DB::SELECT($sql, array($id));

	// If we get more than one item or no items display an error
	if (count($posts) != 1) 
	{
    die("Invalid query or result: $query\n");
  }

	// Extract the first item (which should be the only item)
  $post = $posts[0];
	return $post;
}

// Get a post that a comment has been deleted from
function get_post_2($postsid)
{
  $sql = "SELECT id, posts_author, posts_title, posts_message FROM Posts WHERE id = ?";
	$posts = DB::SELECT($sql, array($postsid));

	// If we get more than one item or no items display an error
	if (count($posts) != 1) 
	{
    die("Invalid query or result: $query\n");
  }

	// Extract the first item (which should be the only item)
  $post = $posts[0];
	return $post;
}

// Get all posts from Posts table
function get_posts() {
	$sql = 'SELECT * FROM Posts ORDER BY id desc';
	$posts = DB::SELECT($sql);

	#return all posts
	return $posts;	
}

// Delete a post
function delete_post($id)
{
  $sql = "DELETE FROM Posts WHERE id = ?";
  $sql2 = "DELETE FROM Comments WHERE postsid = ?";
	DB::SELECT($sql, array($id));
	DB::SELECT($sql2, array($id));
}

// Delete a comment on a given post
function delete_comment($id)
{
	$sql = "DELETE FROM Comments WHERE id = ?";
	DB::SELECT($sql, array($id));
}

// Add a new post
function add_post($author, $title, $message) 
{
  $sql = "INSERT into Posts (posts_author, posts_title, posts_message) values (?, ?, ?)";
	
  $test = DB::INSERT($sql, array($author, $title, $message));
  
  return $test;
}