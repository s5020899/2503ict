drop table if exists Posts;

create table Posts (
	id INTEGER PRIMARY KEY autoincrement,
	number int, /* 0 = subsequent term */
	posts_author VARCHAR(45) DEFAULT '' NOT NULL,
	posts_title VARCHAR(45) DEFAULT '' NOT NULL,
	posts_message TEXT DEFAULT '' NOT NULL);

drop table if exists Comments;

create table Comments (
	id INTEGER PRIMARY KEY autoincrement,
	postsid INT NOT NULL REFERENCES Posts(id),
	comments_author VARCHAR(45) DEFAULT '' NOT NULL,
	comments_message TEXT DEFAULT '' NOT NULL);

insert into posts values (null, "Kevin",  "Website Programming", "Currently working on assignment 1 for website programmnig. Wish me luck!");
insert into posts values (null, "Jimmy", "Dirt Bikes", "Going for a ride to the top of M't Glorious today. Excited is not the word");

insert into comments values (null, "1", 'David', "Goodluck man, I'm sure you'll smash it");
insert into comments values (null, "1", 'Jerry', "Goodluck!");