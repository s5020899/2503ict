@extends('layouts.master')

@section('title')
Associative array search results page
@stop

@section('content')

<h2>Australian Prime Ministers</h2>
<h3>Results for: <?= $search ?></h3>

@if (count($pms) == 0)

<p>No results found.</p>

@else 

<table class="bordered">
<thead>
<tr><th>Name</th><th>From</th><th>To</th><th>Duration</th><th>Party</th><th>State</th></tr>
</thead>
<tbody>

@foreach($pms as $pm)
  </td><td>{{{ $name=$pm->name }}}</td><td>{{{ $start=$pm->start }}}</td><td>{{{ $finish=$pm->finish }}}</td><td>{{{ $duration=$pm->duration }}}</td><td>{{{ $party=$pm->party }}}</td><td>{{{ $state=$pm->state }}}</td></tr>
@endforeach

</tbody>
</table>
@endif

<p><a href="{{ secure_url('/') }}">New search</a></p>
@stop