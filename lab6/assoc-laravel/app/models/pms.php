<?php
/* Australian Prime Ministers.  Data as of 5 March 2010. */
function getpms()
{
  $pms = array(
      array('index' => '27', 'name' => 'Julia Gillard', 'address' => 'Loganlea Rd, Logan', 'phone' => '1234 4321', 'email' => 'julia@the.lodge'),
      array('index' => '28', 'name' => 'Tony Abbot', 'address' => 'Logan Rd, Mt Gravatt', 'phone' => '9876 5432', 'email' => 'tony@the.hermitage'),
      array('index' => '29', 'name' => 'Christine Milne', 'address' => 'Kessels Rd, Nathan', 'phone' => '1234 5678', 'email' => 'chris@the.treetop')
  );
  return $pms;
}
?>

