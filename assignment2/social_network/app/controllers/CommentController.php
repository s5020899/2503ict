<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$v = Validator::make($input, Comments::$rules);
		if($v->passes())
		{
			$comment = new Comments;
			$id = $input['id'];
			$comment->c_id = $id;
			$comment->email = Auth::user()->email;
			$comment->f_name = Auth::user()->f_name;
			$comment->l_name = Auth::user()->l_name;
			$comment->image = Auth::user()->image->url('thumb');
			$comment->message = $input['message'];
			$comment->save();
			Session::put('id', $id);
			return Redirect::to(URL::previous());
		}
		return Redirect::to(URL::previous());

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$id = Session::get('id');
		$post = Posts::find($id);
		$comments = Comments::where('c_id', '=', $id)->get();
		Session::forget('id');
		return View::make('sn.comments')->withPost($post)->withComments($comments);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$comment = Comments::where('id', '=', $id)->delete();
	    return Redirect::to(URL::previous());
	}


}
