<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = Posts::orderBy('id', 'DESC')->get();
		return View::make('sn.feed', compact('posts'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$v = Validator::make($input, Posts::$rules);
		if($v->passes())
		{
			$post = new Posts;
			$email = Auth::user()->email;
			$post->email = $email;
			$post->title = $input['title'];
			$post->message = $input['message'];
			$post->privacy = $input['privacy'];
			$post->image = Auth::user()->image->url('thumb');
			$post->save();
			return Redirect::action('PostController@index');
		}
		return Redirect::action('PostController@index')->withErrors($v);
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Posts::find($id);
		$comments = Comments::orderBy('id', 'DESC')->where('c_id', '=', $id)->get();
		Session::put('id', $id);
		return View::make('sn.comments')->withPost($post)->withComments($comments);
	}		


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = Posts::find($id);
		return View::make('sn.edit', compact('post'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$v = Validator::make($input, Posts::$rules);
		$post = Posts::find($id);
		
		if ($v->passes())
		{	

			$post->title = $input['title'];
			$post->message = $input['message'];
			$post->privacy = $input['privacy'];
			$post->save();
			return Redirect::action('PostController@index',array($post->id));
		}
		else 
		{
			return Redirect::action('PostController@index', array($post->id))->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if (!is_null($id)) {
			Comments::where('c_id', '=', $id)->delete();
			Posts::where('id', '=', $id)->delete();
		    return Redirect::to(URL::previous());
		}
	}
}
