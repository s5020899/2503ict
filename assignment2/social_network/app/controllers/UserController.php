<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$users = User::all();
		return View::make('user.show', compact('users'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Addding a new user
		$input = Input::all();
		$email = strtolower($input['email']);
		$password = $input['password'];
		$encrypted = Hash::make($password);

		$user = new User;
		$user->email = $email;
		$user->password = $encrypted;
		$user->f_name = strtolower($input['f_name']);
		$user->l_name = strtolower($input['l_name']);
		$user->age = $input['age'];
		$user->image = Input::file('image');
		$v = Validator::make($input, User::$rules);

		if($v->passes())
		{
			$user->save();
			return Redirect::to('post');
		}
		else
		{
			return Redirect::route('user.create')->withErrors($v);
		}
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);
		return View::make('user.edit', compact('user'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::find($id);
		$input = Input::all();
		$email = strtolower($input['email']);
		$password = $input['password'];
		$encrypted = Hash::make($password);
		$user->email = $email;
		$user->password = $encrypted;
		$user->f_name = strtolower($input['f_name']);
		$user->l_name = strtolower($input['l_name']);
		$user->age = $input['age'];
		$user->image = Input::file('image');
		$v = Validator::make($input, User::$rules);

		if($v->passes())
		{
			$user->save();
			return Redirect::to('user.index');
		}
		else
		{
			Redirect::to(URL::previous())->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
			User::where('id', '=', $id)->delete();
		    return Redirect::to(URL::previous());

	}
	
	public function login()
	{
		$input = Input::all();
		$password = $input['password'];
		$encrypted = Hash::make($password);
		$email = $input['email'];
		$email = strtolower($email);
		$v = Validator::make($input, User::$login);
		if($v->passes())
		{
			if(Auth::attempt(compact('email', 'password')));
			{
					Session::forget('login_error');
					Session::put('privacy', 'open');
					Redirect::to('post');
			}
		}
	
		Session::put('login_error', 'Login failed');
		return Redirect::to(URL::previous())->withErrors($v);
	}
	
	public function logout()
	{
		Session::forget('privacy');
		Session::forget('login_error');
		Auth::logout();
		return Redirect::action('PostController@index');
	}

	public function search()
	{
		$search = Input::all();
		$search = strtolower($search['search']);
		
		// $users = array();
		// $users = User::where('email', '=', $search)->get();
		// $users = User::where('f_name', '=', $search)->get();
		// $users = User::where('l_name', '=', $search)->get();
		$users = User::where('email', '=', $search)
		            ->orWhere(function($query) use ($search)
            {
                $query->where('f_name', '=', $search);
                $query->orWhere('l_name', '=', $search);
            })
            ->get();
		return View::make('user.search', compact('users'));
	}

}
