<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface {

	use EloquentTrait;
	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	public static $rules = array('email' => 'required|unique:users', 
								'password' => 'required|min:5',
								'age' => 'required',
								'f_name' => 'required',
								'l_name' => 'required'
							);

	public static $login = array('email' => 'required', 'password' => 'required');

	public function __construct(array $attributes = array()) 
	{
		$this->hasAttachedFile('image', ['styles' => ['thumb' => '100x100']]);
		parent::__construct($attributes);
	}
}
