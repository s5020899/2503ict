<?php

class Posts extends Eloquent {
    
	public static $rules = array('title' => 'required|min:5', 'message' => 'required');

    function comments() {
        return $this->hasMany('Comments');
    }
}