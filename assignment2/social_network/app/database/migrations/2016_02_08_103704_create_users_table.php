<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
			Schema::dropIfExists('users');
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password')->index();
			$table->string('f_name');
			$table->string('l_name');
			$table->string('age');
			$table->string('remember_token')->nullable();
			$table->timestamps();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
