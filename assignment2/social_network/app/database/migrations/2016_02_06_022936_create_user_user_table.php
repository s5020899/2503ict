<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('user_user');
		Schema::create('user_user', function($table)
		{
			$table->integer('user_a')->unsigned();
			$table->foreign('user_a')->references('id')->on('users');
			$table->integer('user_b')->unsigned();
			$table->foreign('user_b')->references('id')->on('friends');
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
