@extends('layouts.master')
@section('content')
@section('title')
Search
@stop
@if (count($users))
@foreach($users as $user)
    <table class="table table-hover">
        <tbody class="col-xs-12">
            <tr><th class="col-xs-1">{{ HTML::image($user->image->url('thumb'), 'Profile Picture') }}</th><td></td></tr>
            <tr><th class="col-xs-1">Name: </th><td>{{{ ucwords($user->f_name) }}} {{{ ucwords($user->l_name) }}}</td></tr>
            <tr><th class="col-xs-1">Email: </th><td>{{{ $user->email }}}</td></tr>
            <tr><th class="col-xs-1">Age: </th><td>{{{ ageCalculator($user->age) }}}</td></tr>
            @if (Auth::check() && Auth::user()->email == $user->email)
            <tr><td class="col-xs-1"><b>{{ link_to_route('user.edit', 'Edit', $user->id, array('class' => 'btn btn-info')) }}</b></td>
                <td style="width:30em">
                    {{ Form::open(['method' => 'DELETE', 'route' => ['user.destroy', $user->id]]) }}
                    {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'style' => 'margin-left:1em']) }}
                    {{ Form::close() }}
                </td></tr
            @endif
        </tbody>
    </table>
@endforeach
@else
<p style="font-weight:bold;color:red;margin:10px">There are no users that match your query. You may search by email, first name or last name.</p>
@endif
@stop