@extends('layouts.master')
@section('title')
Edit Profile
@stop
@section('content')
<h3 style="margin:10px;">Edit Post</h3>
{{ Form::model($user, array('method' => 'PUT', 'class' => 'well', 'route' => array('user.update', $user->id))) }}
    <div class="form-group">
        {{ Form::label('email', 'Email:') }}
        {{ $errors->first('email','<small class=error>:message</small>') }}
        {{ Form::email('email', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">        
        {{ Form::label('password', 'Password: ') }}
        {{ $errors->first('password', '<small class=error>:message</small>') }}
        {{ Form::password('password', array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('f_name','First Name: ') }}
        {{ Form::text('f_name', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('l_name','Last Name: ') }}
        {{ Form::text('l_name', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('age','Age: ') }}
        {{ Form::input('date', 'age') }}
    </div>
    <div class="form-group">
        {{ Form::label('image','Profile Picture: ') }}
        {{ Form::file('image') }}
    </div>
        {{ Form::submit('Save', array('class' => 'createsub btn btn-info')) }}
		{{ link_to_route('user.search', 'Cancel', array('class' => 'btn btn-info')) }}
</form>
{{ Form::close() }}
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
@stop