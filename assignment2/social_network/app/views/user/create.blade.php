@extends('layouts.master')
@section('content')
@section('title')
Login / Signup
@stop
@if (!Auth::check())
 	{{ Form::open(array('route' => 'user.login', 'class' => 'create well'))  }}
 	    {{ Session::get('login_error') }}
	    <div class="form-group">
	        {{ Form::label('email', 'Email:') }}
	        {{ Form::email('email', null, array('class' => 'form-control')) }}
	        
	    </div>
	    <div class="form-group">        
	        {{ Form::label('password', 'Password:') }}
	        {{ Form::password('password', array('class' => 'form-control')) }}
	    </div>
	        {{ Form::submit('Login', array('class' => 'createsub btn btn-info')) }}
	{{ Form::close() }}
@endif

<!--Signup Form-->

{{ Form::open(array('action' => 'UserController@store', 'class'=>'create well', 'files' => true)) }}
    <div class="form-group">
        {{ Form::label('email', 'Email: ') }}
        {{ Form::email('email', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">        
        {{ Form::label('password', 'Password: ') }}
        {{ Form::password('password', array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('f_name','First Name: ') }}
        {{ Form::text('f_name', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('l_name','Last Name: ') }}
        {{ Form::text('l_name', null, array('class' => 'form-control')) }}
    </div>    
    <div class="form-group">
        {{ Form::label('age','Age: ') }}
        {{ Form::input('date', 'age') }}
    </div>
    <div class="form-group">
        {{ Form::label('image','Profile Picture: ') }}
        {{ Form::file('image') }}
    </div>
        {{ Form::submit('Register', array('class' => 'createsub btn btn-info')) }}
{{ Form::close() }}
@stop