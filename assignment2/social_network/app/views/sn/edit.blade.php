@extends('layouts.master')
@section('title')
Edit Post
@stop
@section('content')
<h3 style="margin:10px;">Edit Post</h3>
{{ Form::model($post, array('method' => 'PUT', 'class' => 'well', 'route' => array('post.update', $post->id))) }}
    <div class="form-group">
        {{ Form::label('title', 'Title:') }}
        {{ $errors->first('title','<small class=error>:message</small>') }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('message', 'Message:', array('style' => 'float:left;')) }}
        {{ $errors->first('message','<small class=error>:message</small>') }}
        {{ Form::textarea('message', null, array('class'=>'form-control', 'rows'=>'4')) }}
    </div>
    <div class="form-group">
        {{ Form::select('privacy', array('1' => 'Public', '2' => 'Friends', '3' => 'Private'),'2') }}
    </div>
        {{ Form::submit('Save', array('class' => 'createsub btn btn-info')) }}
		{{ link_to_route('post.index', 'Cancel', $post->id, array('class' => 'btn btn-info')) }}
</form>
{{ Form::close() }}
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
@stop