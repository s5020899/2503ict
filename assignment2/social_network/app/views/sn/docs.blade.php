@extends('layouts.master')

@section('title')
Documentation
@stop
@section('content')

<h1>Documentation</h1>

<h4>Entity Relationship Diagram</h4>
{{ HTML::image('images/entity_relationship_diagram.png', 'entity relationship diagram', array( 'width' => 375, 'height' => 200 )) }}

<h4>Website Routes</h4>
{{ HTML::image('images/website_diagram.png', 'website diagram1', array( 'width' => 370, 'height' => 240 )) }}
{{ HTML::image('images/website_diagram2.png', 'website diagram2', array( 'width' => 370, 'height' => 240 )) }}

<ol style="margin:10px">
    <li>I was unable to complete any <b>friends features</b> at this point.</li>
    <li><b>Unable</b> to update profile picture</li>
    <li>Users <b>don't</b> exactly have a profile page</li>
    <li><b>Pagination not completed</b></li>
    <li><b>Approach I took: </b>Work as hard as possible. I beleieve the majority of this assignment was achieved by reading lecture notes and completing each lab</li>
    <li>I spent many hours working on this each day, in the end I just ran out of time.</li>
</ol>
@stop