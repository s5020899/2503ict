@extends('layouts.master') 
@section('title')
Add Comment
@stop
@section('content')

        <div class="col-xs-12" style="padding:10px;">
            <table class="table table-hover">
                <tbody class="col-xs-12">
                    <tr><th class="col-xs-1">{{ HTML::image($post->image, 'profile picture') }}</th><td></td></tr>
                    <tr><th class="col-xs-1">Email: </th><td>{{{ $post->email }}}</td></tr>
                    <tr><th class="col-xs-1">Title: </th><td>{{{ $post->title }}}</td></tr>
                    <tr><th class="col-xs-1">Message: </th><td>{{{ $post->message }}}</td></tr>
                </tbody>
            </table>
            <hr>
            @if(Auth::check())
            <h3 style="padding:10px;">Comment on Post</h3>
            {{Form::open(array('method'=>'POST','route'=> array('comment.store',$post->id), 'class' => 'create well', 'style' => 'margin:0px 10px'))}}
                <div class="form-group">
                    {{ Form::hidden('id', $post->id) }}
                    {{ Form::label('message', 'Message:', array('style' => 'float:left;')) }}
                    {{ Form::textarea('message', null, array('class'=>'form-control', 'rows'=>'4')) }}
                </div>
                    {{ Form::submit('Add Comment', array('class' => 'createsub btn btn-info')) }}
            {{ Form::close() }}
            @endif
            
            <h3 style="padding:10px;">Comments</h3>
            @if(isset($comments))
            @foreach($comments as $comment) 
            <table class="table table-hover">
                <tbody class="col-xs-12">
                    <tr><th>@if($comment->image){{ HTML::image($comment->image, 'profile picture') }}@endif</th><td></td></tr>
                    <tr><th class="col-xs-1">Author: </th><td>@if($comment->f_name && $comment->l_name){{{ $comment->f_name }}} {{{ $comment->l_name }}}@else {{{ $comment->email }}}@endif</td></tr>
                    <tr><th class="col-xs-1">Message: </th><td>{{{ $comment->message }}}</td></tr>
                    
                    <tr><th class="col-xs-1">
                        @if(Auth::check() && Auth::user()->email == $comment->email)
                        {{ Form::open(['method' => 'DELETE', 'route' => ['comment.destroy', $comment->id]]) }}
                        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                        {{ Form::close() }}</th>@endif<td></td></tr>
                </tbody>
            </table>
            @endforeach
            @else
            <p>No comments yet.</p>
            @endif
            
</div>
@stop

