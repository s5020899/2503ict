@extends('layouts.master') 
@section('title')
News Feed
@stop
@section('content')
@if (!Auth::check())
 	{{ Form::open(array('route' => 'user.login', 'class' => 'create well'))  }}
 	    {{ Session::get('login_error') }}
	    <div class="form-group">
	        {{ Form::label('email', 'Email') }}
	        {{ Form::email('email', null, array('class' => 'form-control')) }}
	    </div>
	    <div class="form-group">        
	        {{ Form::label('password', 'Password') }}
	        {{ Form::password('password', array('class' => 'form-control')) }}
	    </div>
	        {{ Form::submit('Login', array('class' => 'createsub btn btn-info')) }}
	{{ Form::close() }}
@endif

<!--Form to add a new post-->
<h3 style="margin:10px;"></h3>
@if (Auth::check())
<h3 style="margin:10px;">Create Post</h3>                	
{{ Form::open(array('POST' => 'PostController@store', 'class' => 'create well', 'style' => 'margin:0px 10px')) }}
    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('message', 'Message', array('style' => 'float:left;')) }}
        {{ Form::textarea('message', null, array('class'=>'form-control', 'rows'=>'4')) }}
    </div>
    <div class="form-group">
        {{ Form::select('privacy', array('1' => 'Public', '2' => 'Friends', '3' => 'Private'),'2') }}
    </div>
        {{ Form::submit('Create', array('class' => 'createsub btn btn-info')) }}
{{ Form::close() }}
@endif
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif
<h3 style="margin:10px;">News Feed</h3>   
                            @if (count($posts))
                            
                            <!--Loop for each Post in Posts Table if user is logged in -->
                         
                            @foreach($posts as $post)
                        
                                @if ((Session::get('privacy') == 'open') && ($post->privacy != 3)) 
                                <table class="table table-hover">
                                    <tbody class="col-xs-12">
                                        <tr><th class="col-xs-1">{{ HTML::image($post->image, 'profile picture') }}</th><td></td></tr>
                                        <tr><th class="col-xs-1">Email: </th><td>{{{ $post->email }}}</td></tr>
                                        <tr><th class="col-xs-1">Title: </th><td>{{{ $post->title }}}</td></tr>
                                        <tr><th class="col-xs-1">Message: </th><td>{{{ $post->message }}}</td></tr>
                                        <tr><td class="col-xs-1">{{ link_to_route('post.show', 'Comments', $post->id, array('class' => 'btn btn-info', 'style' => 'float:left')) }}</td>
                                            <!--If user is authorised and if user email = the posts email field-->
                                            @if ((Auth::check()) && (Auth::user()->email == $post->email))
                                            <td style="width:30em">{{ link_to_route('post.edit', 'Edit', $post->id, array('class' => 'btn btn-info', 'style' => 'float:left')) }}
                                            {{ Form::open(['method' => 'DELETE', 'route' => ['post.destroy', $post->id]]) }}
                                            {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'style' => 'margin-left:1em; float:left']) }}
                                            {{ Form::close() }}</td></tr>
                                            @else<td></td></tr>
                                    </tbody>
                                </table>
                                @endif
                                @if(($post->priavcy == 3) && (Auth::check()) && (Auth::user()->email == $post->email))
                                <table class="table table-hover">
                                    <tbody class="col-xs-12">
                                        <tr><th class="col-xs-1">{{ HTML::image($post->image, 'profile picture') }}</th><td></td></tr>
                                        <tr><th class="col-xs-1">Email: </th><td>{{{ $post->email }}}</td></tr>
                                        <tr><th class="col-xs-1">Title: </th><td>{{{ $post->title }}}</td></tr>
                                        <tr><th class="col-xs-1">Message: </th><td>{{{ $post->message }}}</td></tr>
                                        <tr><td class="col-xs-1">{{ link_to_route('post.show', 'Comments', $post->id, array('class' => 'btn btn-info', 'style' => 'float:left')) }}</td>
                                            <td style="width:30em">{{ link_to_route('post.edit', 'Edit', $post->id, array('class' => 'btn btn-info', 'style' => 'float:left')) }}
                                            {{ Form::open(['method' => 'DELETE', 'route' => ['post.destroy', $post->id]]) }}
                                            {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'style' => 'margin-left:1em; float:left']) }}
                                            {{ Form::close() }}</td></tr>
                                    </tbody>
                                </table> 
                                @endif
                            <!--Loop for each Post in Posts Table if user is not logged in-->
                            @elseif(Session::get('privacy') != 'open')
                            @if ($post->privacy == 1)
                                <table class="table table-hover">
                                    <tbody class="col-xs-12">
                                        <tr><th class="col-xs-1">{{ HTML::image($post->image, 'profile picture') }}</th><td></td></tr>
                                        <tr><th class="col-xs-1">Email: </th><td>{{{ $post->email }}}</td></tr>
                                        <tr><th class="col-xs-1">Title: </th><td>{{{ $post->title }}}</td></tr>
                                        <tr><th class="col-xs-1">Message: </th><td>{{{ $post->message }}}</td></tr>
                                        <tr><td class="col-xs-1">{{ link_to_route('post.show', 'Comments', $post->id, array('class' => 'btn btn-info', 'style' => 'float:left')) }}</td>
                                        <td style="width:30em">
                                            <!--If the user is authorised-->
                                            @if (Auth::check())
                                            {{ link_to_route('post.edit', 'Edit', $post->id, array('class' => 'btn btn-info')) }}
                                            {{ Form::open(['method' => 'DELETE', 'route' => ['post.destroy', $post->id]]) }}
                                            {{ Form::submit('Delete', ['class' => 'btn btn-danger', 'style' => 'margin-left:1em']) }}
                                            {{ Form::close() }}</td></tr>@endif
                                    </tbody>
                                </table>
                            @endif
                            @endif
                            @endforeach
                            @else
                            <p>It's like a ghost town here.</p>
                            @endif
                            
@stop