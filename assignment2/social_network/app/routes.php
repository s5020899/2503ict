<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});


Route::resource('post', 'PostController');
Route::resource('comment','CommentController');
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
Route::post('user/search', array('as' => 'user.search', 'uses' => 'UserController@search'));
Route::get('docs', function()
{
  return View::make('sn.docs');
});
Route::resource('user', 'UserController');
Route::get('/', array('as'=>'home', 'uses'=> 'PostController@index'));


// Put this in the model
function ageCalculator($dob){
		if(!empty($dob)){
			$birthdate = new DateTime($dob);
			$today   = new DateTime('today');
			$age = $birthdate->diff($today)->y;
			return $age;
		}else{
			return 0;
		}
}