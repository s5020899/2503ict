<?php
/*
 * Script to print the prime factors of a single positive integer
 * sent from a form.
 * BAD STYLE: Does not use templates.
 */
include "includes/defs.php";

# Set $number to the value entered in the form.
$number = $_GET['number'];

# Check $number is nonempty, numeric and between 2 and PHP_MAX_INT = 2^31-1.
# (PHP makes it difficult to do this naturally; see the manual.)
if (empty($number)) {
    $err = "<p class='alert'>Error: Missing value\n</p>";
    //exit;
} else if (!is_numeric($number)) {
    $err = "<p class='alert'>Error: Nonnumeric value: $number\n</p>";
    //exit;
} else if ($number < 2 || $number != strval(intval($number))) {
    $err = "<p class='alert'>Error: Invalid number: $number\n</p>";
    //exit;
} else {
    # Set $factors to the array of factors of $number.
    $factors = factors($number);
    # Set $factors to a single dot-separated string of numbers in the array.
    $factors = join(" . ", $factors);
    if(!empty($factors)) {
    $num = "<p>$number = $factors</p>";
    }
}?>

<!DOCTYPE HTML>
<!-- Display the factors. -->
<html>
  <head>
    <title>Factors</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="styles/wp.css">
  </head>
  
  <body>
    <h1>Factorisation</h1>

    <!--Error goes here!-->
    <?= $err ?>
    
    <!--Factors Here-->
    <?= $num ?>

    <h2>Another factorisation</h2>

    <form method="get" action="factorise.php">
      <p>Number to factorise: 
         <input type="text" name="number" value="<?=$number?>">
      <p><input type="submit" value="Factorise it!">
    </form>
    
  </body>
</html>
