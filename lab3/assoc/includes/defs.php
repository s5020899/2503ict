<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($search) {
    global $pms; 

    // Filter $pms by $name
    if (!empty($search)) {
	$results = array();
	foreach ($pms as $pm) {
	    if (stripos($pm['name'], $search) !== FALSE) {
		$results[] = $pm;
	    }
	    else if(strpos($pm['from'], $search) !== FALSE || 
	    strpos($pm['to'], $search) !== FALSE) {
		$results[] = $pm;
	    }
	    else if (stripos($pm['state'], $search) !== FALSE) {
		$results[] = $pm;
	    }

	}
	$pms = $results;
    }

    return $pms;
}
?>
