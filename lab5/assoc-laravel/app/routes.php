<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell pmravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/pms.php";


// Disppmy search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and disppmy results
Route::get('search', function()
{
  $search = Input::get('search');

  $results = search($search);

	return View::make('pms.results')->withPms($results)->withSearch($search);
});


/* Functions for pm database example. */

/* Search sample data for $name or $year or $state from form. */
function search($search) {
  $pms = getpms();

  // Filter $pms by $name
  if (!empty($search)) {
    $results = array();
  	foreach ($pms as $pm) {
  	    if (stripos($pm['name'], $search) !== FALSE) {
  		    $results[] = $pm;
  	    }
  	    else if (stripos($pm['address'], $search) !== FALSE) {
  	      $results[] = $pm;  
  	    }
  	    else if (stripos($pm['phone'], $search) !== FALSE) {
  	      $results[] = $pm;
  	    }
  	    else if (stripos($pm['email'], $search) !== FALSE) {
  	      $results[] = $pm;
  	    }
    }
    $pms = $results;
  }

  return $pms;
}